Selenium Tech Test
by Maslovsky Mikhail

- list of BDD tests you can find here: src\src\test\resources\features
- see Idea project TestT@sk.iml for details.
- reports you can find here src\target\cucumber-reports\advanced-reports\cucumber-html-reports\overview-features.html
- For qick run tests and open reports(for WindowsOS with Java JDK and maven installed)use run_test.cmd

Thank you in advance!
Mikhail