package com.mikm.testtask.cucumber;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import com.mikm.testtask.helpers.Elements;
import cucumber.api.java.en.When;
import org.testng.log4testng.Logger;
import com.mikm.testtask.pom.ActivePage;
import com.mikm.testtask.pom.CompletedPage;
import com.mikm.testtask.pom.MainPage;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

//cucumber steps class
public class CucumberSteps {
    private MainPage mainPage = null;
    private ActivePage activePage = null;
    private CompletedPage completedPage = null;
    private static final Logger log = Logger.getLogger(CucumberSteps.class);
    private Elements elements = null;
    private List<String> listOfTaskNames = null;
    private List<String> listOfTaskNamesForCompletion = null;
    private List<String> listOfTaskNamesForCheck= null;

    //Open browser
    @Before
    public void setOpenedMainPage() {
        this.mainPage = new MainPage("chrome");
        this.activePage = new ActivePage(this.mainPage.getWdBrowser());
        this.completedPage = new CompletedPage(this.mainPage.getWdBrowser());
    }

    //open main page
    @Given("^Main page opened")
    public void pageOpened() {
        this.mainPage.getPage("http://todomvc.com/examples/angularjs/#/");
        this.elements = this.mainPage.getElements();
    }

    @Given("^navigate to All list")
    public void navigate_to_All_list() {
        this.elements.navigateToAllList();
    }

    @Given("^navigate to Active list")
    public void navigate_to_Active_list() {
        this.elements.navigateToActiveList();
    }

    @Given("^navigate to Completed list")
    public void navigate_to_Completed_list() {
        this.elements.navigateToCompletedList();
    }

    @Given("^list of task names$")
    public void list_of_task_names(List<String> listOfTaskNames ) {
        this.listOfTaskNames=listOfTaskNames;
    }
    @Given("^list of task names for completion$")
    public void list_of_task_names_for_completion(List<String> listOfTaskNamesForCompletion)  {
        this.listOfTaskNamesForCompletion= listOfTaskNamesForCompletion;
    }

    @Given("^list of task for check$")
    public void list_of_task_for_check(List<String> listOfTaskNamesForCheck) {
        this.listOfTaskNamesForCheck= listOfTaskNamesForCheck;
    }

    /**
     * Submit list of task from  private List<String> listOfTaskNames
     */
    @When("^submit given tasks$")
    public void submit_given_tasks() {
      for(String taskName:this.listOfTaskNames){
          submit_task_name(taskName);
      }
    }

    /**
     * Mark tasks as comleted from private List<String> listOfTaskNamesForCompletion
     */
    @When("^mark given tasks for completion as completed$")
    public void mark_given_tasks_for_completion_as_completed() {
        for(String taskName: this.listOfTaskNamesForCompletion){
            mark_task_as_completed(taskName);
        }

    }
    /**
     * Check tasks are presented from private List<String> listOfTasksForCheck
     */
    @Then("^Check tasks from list of task for check are presented$")
    public void check_tasks_from_list_of_task_for_check_are_presented() {
        for(String listOfTasksForCheck: this.listOfTaskNamesForCheck){
            check_task_is_presented(listOfTasksForCheck);
        }
    }

    /**
     * submit a task with name
     * @param taskName
     */
    @Then("^submit task name.*\"([^\"]*)\"$")
    public void submit_task_name(String taskName) {
        this.mainPage.submitTask(taskName);
    }

    @Then("^Check task \"([^\"]*)\" is presented$")
    public void check_task_is_presented(String taskName) {
        assertTrue(mainPage.isTaskPresent(taskName));
    }

    @Then("^Check task \"([^\"]*)\" is absent$")
    public void check_task_is_Absent(String taskName) {
        assertTrue(mainPage.isTaskAbsent(taskName));
    }

    // mark task as completed by name
    @Then("^mark task \"([^\"]*)\" as completed$")
    public void mark_task_as_completed(String taskName) {
        this.mainPage.markTaskAsCompleted(taskName);
    }

    // mark task as uncompleted by name
    @Then("^mark task \"([^\"]*)\" as uncompleted$")
    public void mark_task_as_uncompleted(String taskName) {
        this.mainPage.markTaskAsUncompleted(taskName);
    }

    @Then("^Check task \"([^\"]*)\" is marked as completed$")
    public void check_task_is_marked_as_completed(String taskName) {
        assertTrue(this.mainPage.isTaskCompeted(taskName));
    }

    @Then("^Check task \"([^\"]*)\" is marked as uncompleted")
    public void check_task_is_marked_as_uncompleted(String taskName) {
        assertTrue(this.mainPage.isTaskUncompeted(taskName));
    }

    @Then("^Check (\\d+) tasks is in Active list$")
    public void check_tasks_is_in_Active_list(int number) {
        this.elements.navigateToActiveList();
        assertEquals(this.elements.countTasksInList(),number);
    }

    @Then("^Check (\\d+) tasks is in Completed list$")
    public void check_tasks_is_in_Completed_list(int number) {
        this.elements.navigateToCompletedList();
        assertEquals(this.elements.countTasksInList(),number);
    }

    @Then("^Check (\\d+) tasks is in All list$")
    public void check_tasks_is_in_All_list(int number) {
        this.elements.navigateToAllList();
        assertEquals(this.elements.countTasksInList(),number);
    }

    @Then("^remove task \"([^\"]*)\"$")
    public void remove_task(String taskName) {
        this.elements.removeTask(taskName);
    }
    @After
    public void closePage() {
        this.mainPage.close();
    }
}