package com.mikm.testtask.pom;

import com.mikm.testtask.helpers.WDBrowser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.log4testng.Logger;

// main page page object class
public class MainPage extends WDBrowser {
    private static final Logger log = Logger.getLogger(MainPage.class);
    private WDBrowser wdBrowser;
    final private By field = By.xpath(".//input[@id=\"new-todo\"]");
    private String url;

    public MainPage(String browser) {
        setWdBrowser(super.openBrowser(browser));
    }

    private void setWdBrowser(WDBrowser wdBrowser) {
        this.wdBrowser = wdBrowser;
    }

    public WebElement submitTask(String taskText) {
        log.debug("\n---- Create item:" + taskText);
        return enterValueAndSubmit(taskText, field);
    }

    // try to find Task on page
    public WebElement tryToFindTask(String taskText) {
        log.debug("\n---- Is task:" + taskText + " presented");
        WebElement webElement = null;
        try {
            webElement = findElementWithWait(By.xpath(".//label[.='" + taskText + "']"));
        } catch (Exception E) {
            return null;
        }
        return webElement;

    }

    public boolean isTaskPresent(String taskText) {
        return tryToFindTask(taskText) != null;
    }

    public boolean isTaskAbsent(String taskName) {
        return isElementAbsent(By.xpath(".//label[.='" + taskName + "']")) != null;
    }

    public WebElement markTaskAsCompleted(String taskName) {
        log.debug("\n---- Complete task:" + taskName);
        String xpathToFind = "//li[@class=\"ng-scope\"]/div/label[.='" + taskName + "']/preceding-sibling::input";
        WebElement webElement = null;
        try {
            webElement = findElementWithWait(By.xpath(xpathToFind));
        } catch (Exception E) {
            System.err.println("Element wasn't found! By:" + xpathToFind + "\n");
            return null;
        }
        webElement.click();
        return webElement;

    }

    public WebElement markTaskAsUncompleted(String taskName) {
        log.debug("\n---- uncompleted task:" + taskName);
        String xpathToFind = "//li[@class=\"ng-scope completed\"]/div/label[.='" + taskName + "']/preceding-sibling::input";
        WebElement webElement = null;
        try {
            webElement = findElementWithWait(By.xpath(xpathToFind));
        } catch (Exception E) {
            System.err.println("Element wasn't found! By:" + xpathToFind + "\n");
            return null;
        }
        webElement.click();
        return webElement;
    }

    public WebElement tryIsTaskCompleted(String taskName) {
        log.debug("\n---- Try task:" + taskName + " Completed");
        String xpathToFind = "//li[@class=\"ng-scope completed\"]/div/label[.='" + taskName + "']/preceding-sibling::input";
        WebElement webElement = null;
        try {
            webElement = findElementWithWait(By.xpath(xpathToFind));
        } catch (Exception E) {
            System.err.println("Element wasn't found! By:" + xpathToFind + "\n");
            return null;
        }
        return webElement;
    }

    public WebElement tryIsTaskUncompleted(String taskName) {
        log.debug("\n---- Try task:" + taskName + " Uncompleted");
        String xpathToFind = "//li[@class=\"ng-scope\"]/div/label[.='" + taskName + "']/preceding-sibling::input";
        WebElement webElement = null;
        try {
            webElement = findElementWithWait(By.xpath(xpathToFind));
        } catch (Exception E) {
            System.err.println("Element wasn't found! By:" + xpathToFind + "\n");
            return null;
        }
        return webElement;
    }

    public Boolean isTaskCompeted(String taskName) {
        if (tryIsTaskCompleted(taskName) == null) {
            log.debug("\n---- Task:" + taskName + " Uncompleted");
            return false;
        } else { return true;}
    }

    public Boolean isTaskUncompeted(String taskName) {
        if (tryIsTaskUncompleted(taskName) == null) {
            log.debug("\n---- Task:" + taskName + " Completed");
            return false;
        } else { return true;}
    }

    public WDBrowser getWdBrowser() {
        return wdBrowser;
    }
}



