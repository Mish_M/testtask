package com.mikm.testtask.pom;

import com.mikm.testtask.helpers.WDBrowser;
import org.testng.log4testng.Logger;

public class CompletedPage extends WDBrowser {
    private static final Logger log = Logger.getLogger(CompletedPage.class);
    private final WDBrowser wdBrowser;

    public CompletedPage(WDBrowser wdBrowser) {
        this.wdBrowser = wdBrowser;
    }
}