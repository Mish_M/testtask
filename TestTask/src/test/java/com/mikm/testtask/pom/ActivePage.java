package com.mikm.testtask.pom;

import com.mikm.testtask.helpers.WDBrowser;
import org.testng.log4testng.Logger;

public class ActivePage extends WDBrowser {
    private static final Logger log = Logger.getLogger(CompletedPage.class);
    private final WDBrowser wdBrowser;

    public ActivePage(WDBrowser wdBrowser) {
        this.wdBrowser = wdBrowser;
    }

}
