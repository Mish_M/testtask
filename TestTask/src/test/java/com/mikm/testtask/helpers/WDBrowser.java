package com.mikm.testtask.helpers;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.log4testng.Logger;

//Class to work with browsers
public class WDBrowser extends Elements {

    private static final Logger log = Logger.getLogger(WDBrowser.class);

    public WDBrowser openBrowser(String browser) {
        //TODO add exception on browser start
        // Select and open browser
        try {
            if (browser.toLowerCase().contentEquals("*firefox")) {
                super.setDriver(new FirefoxDriver());
                log.debug("---- Open FF");
            } else if (browser.toLowerCase().contentEquals("chrome")) {
                super.setDriver(new ChromeDriver());
                log.debug("---- Open Chrome");
            } else {
                //TODO throw exception "wrong browser"
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }
}
