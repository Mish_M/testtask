package com.mikm.testtask.helpers;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

//class fo common used methods
public abstract class Elements {
    private WebDriver driver;
    private static final Logger log = Logger.getLogger(Elements.class);

    public WebDriver getDriver() {
        return this.driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    //Pause method
    public void pause(int milisec) {
        log.debug("\n---- !!Pause:" + milisec);
        try {
            Thread.sleep(milisec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Get page by URL
    public void getPage(String url) {
        log.debug("\n---- Open page:" + url);
        getDriver().get(url);
    }

    //Close browser
    public void close() {
        driver.quit();
        log.debug("\n---- Close page:\n");
    }

    //enter text to field with text to field.
    public WebElement enterValueAndSubmit(String textToEnter, By field) {
        WebElement webElement = findElementWithWait(field);
        webElement.click();
        webElement.sendKeys(textToEnter);
        webElement.submit();
        return webElement;

    }

    //Find with timeout
    public WebElement findElementWithWait(By by, Integer timeOut) {
        return (new WebDriverWait(getDriver(), timeOut))
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    //Find and wait with predefined timeout
    public WebElement findElementWithWait(By xpath) {
        return findElementWithWait(xpath, 15);
    }

    //Is element absent
    public Boolean isElementAbsent(By xpath) {
        try {
            findElementWithWait(xpath, 1);
        } catch (Exception ee) {
            log.error("Element" + xpath.toString() + "is presented");
            return true;
        }

        return false;
    }

    public WebElement clickButtonByText(String buttonCaption) {
        WebElement webElement = findElementWithWait(By.xpath(".//a[.='" + buttonCaption + "']"));
        webElement.click();
        return webElement;
    }

    public void navigateToCompletedList() {
        clickButtonByText(NavigationMenuHelper.getCompletedButton());
    }

    public void navigateToActiveList() {
        clickButtonByText(NavigationMenuHelper.getActiveButton());
    }

    public void navigateToAllList() {
        clickButtonByText(NavigationMenuHelper.getAllButton());
    }

    public Elements getElements() {
        return this;
    }

    public void removeTask(String taskName) {
        Actions action = new Actions(getDriver());
        // Find task by name
        WebElement task = findElementWithWait(By.xpath("//label[.='" + taskName + "']"));
        // Move mouse over it to cross became visible
        action.moveToElement(task).pause(10).build().perform();
        // Find and click on cross button
        findElementWithWait(By.xpath("//label[.='" + taskName + "']/following-sibling::button")).click();
    }
    public int countTasksInList() {
        return getDriver().findElements(By.xpath("//label[@class='ng-binding']")).size();
    }


}
