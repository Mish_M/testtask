package com.mikm.testtask.helpers;

public class NavigationMenuHelper {
    private static final String AllButton = "All";
    private static final String CompletedButton = "Completed";
    private static final String ActiveButton = "Active";

    public static String getActiveButton() {
        return ActiveButton;
    }

    public static String getAllButton() {
        return AllButton;
    }

    public static String getCompletedButton() {
        return CompletedButton;
    }
}
