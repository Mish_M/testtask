Feature: HTML Reporting test.
	As a customer, 
	I want to see HTML reporting that clearly shows where in the users’ path a problem occurred.
Scenario: Failed test with mistake in test data to test reporting. 
	Given Main page opened
	Given list of task names
	|Task for completion1|
	|Task for completion2|
	|Task for completion3|
	|Active Task1|
	|Active Task2|
	|Active Task3|
	|Active Task4|
	Given list of task names for completion
	|Task for completion1|
	|Task for completion2|
	|Task for completion3|
	When submit given tasks
	And mark given tasks for completion as completed
	Then Check 12 tasks is in Active list
	Then Check 3 tasks is in Completed list
	Then Check 7 tasks is in All list