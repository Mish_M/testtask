Feature: Remove task.
	As a customer, I want to remove task by clicking on the cross button
Scenario: Successful add tasks
	Given Main page opened
  	When submit task name "Active Task1"
	And submit task name "Active Task2 to remove"
	And remove task "Active Task2 to remove"
	Then Check task "Active Task1" is absent
	And Check 1 tasks is in All list