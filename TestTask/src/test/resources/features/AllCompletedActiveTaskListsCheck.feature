Feature: All Completed Active task lists check.
	As a customer, I want to see lists of All, Completed and Active task
Scenario: Successful add tasks
	Given Main page opened
	And list of task names
	|Task for completion1|
	|Task for completion2|
	|Task for completion3|
	|Active Task1|
	|Active Task2|
	|Active Task3|
	|Active Task4|
	And list of task names for completion
	|Task for completion1|
	|Task for completion2|
	|Task for completion3|
    When submit given tasks
	And mark given tasks for completion as completed
	Then Check 4 tasks is in Active list
	And Check 3 tasks is in Completed list
	And Check 7 tasks is in All list