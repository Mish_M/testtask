Feature: Lists navigation
		As a customer,
        I want to navigate through All Active and Completed lists
 Scenario: All Active and Completed lists navigation.
		Given Main page opened
		When submit task name "Task for completion1"
		When submit task name "Task for completion2"
		And submit task name "Active Task1"
		And mark task "Task for completion1" as completed
		And mark task "Task for completion2" as completed
		And navigate to Completed list
		Then Check 2 tasks is in Completed list
		When navigate to All list
		Then Check 3 tasks is in All list
		When navigate to Active list
		Then Check 1 tasks is in Active list