Feature: Multiple Task Creation
		As a customer,
        I want to create multiple task
Scenario: Successful add several tasks
	Given Main page opened
	And list of task names
	|SuccessfulTask1|
	|SuccessfulTask2|
	|SuccessfulTask3|	
	|SuccessfulTask4|	
	|SuccessfulTask5|	
	And list of task for check
	|SuccessfulTask1|
	|SuccessfulTask2|
	|SuccessfulTask3|	
	|SuccessfulTask4|	
	|SuccessfulTask5|	
	When submit given tasks
	Then Check tasks from list of task for check are presented