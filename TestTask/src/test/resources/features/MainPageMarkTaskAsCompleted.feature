Feature: Mark task as completed, uncompleted.
		As a customer,
        I want to mark active task as completed 
		I want to mark completed task as active 
 Scenario: Mark task as completed, uncompleted.
		Given Main page opened
		When submit task name "Task to mark as completed"
		And submit task name "Task to mark as completed an uncompleted it then"
		And mark task "Task to mark as completed" as completed
		Then Check task "Task to mark as completed" is marked as completed
		When mark task "Task to mark as completed an uncompleted it then" as completed
		Then Check task "Task to mark as completed an uncompleted it then" is marked as completed
		When mark task "Task to mark as completed an uncompleted it then" as uncompleted
		Then Check task "Task to mark as completed an uncompleted it then" is marked as uncompleted	