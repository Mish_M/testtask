# README #

#### Selenium Tech Test by Maslovsky Mikhail ####

## Quick summary ##
This project contain several BBD tests of http://todomvc.com/examples/angularjs/#/
####Deliverables####

1. BDD style (Given, when and then) selenium web driver backed test scenarios. 
2. Utilization of the PageObject pattern and any other patterns you deem appropriate. 
3. HTML reporting that clearly shows where in the users� path a problem occurred. 
4. git repository

#### List of tests ####

1. All Completed Active task lists check.
2. HTML Reporting test. -- Should be failed to show how error reporting works
3. Lists navigation
4. Remove task.
5. Add task.
6. Mark task as completed, uncompleted.
7. Multiple Task Creation

* Version
beta

### How do I get set up? ###
* download and unpack project(JDK and maven should be installed properly)) 
* How to run tests

mvn clean verify

or

run_test.cmd[windows only]

or 

open idea project with testNG plugin installed and run testng_CU.xml //drivers for appropriate OS and browser should be placed in the root of project

### Some more details ###
* list of BDD tests you can find here: TestTask\src\test\resources\features\*.features
* see Idea project TestTask for details.
* reports you can find here TestTask\target\cucumber-reports\advanced-reports\cucumber-html-reports\overview-features.html
* For qick run tests and open reports(for WindowsOS with Java JDK and maven installed)use run_test.cmd

### Who do I talk to? ###

* Repo owner or admin 
Maslovsky Mikhail
maslovsky.mikhail@gmail.com
